

function registerPushwooshIOS() {
 	var pushNotification = window.plugins.pushNotification;


	document.addEventListener('push-notification',
		function(event)
		{
			var notification = event.notification;

			alert(notification.aps.alert);


			pushNotification.setApplicationIconBadgeNumber(0);
		}
	);

    pushNotification.onDeviceReady({pw_appid:"539E9-AB8AE"});

	pushNotification.registerDevice(
		function(status)
		{
			var deviceToken = status['deviceToken'];
			console.warn('registerDevice: ' + deviceToken);
			onPushwooshiOSInitialized(deviceToken);
		},
		function(status)
		{
			console.warn('failed to register : ' + JSON.stringify(status));
		}
	);

	pushNotification.setApplicationIconBadgeNumber(0);
}

function onPushwooshiOSInitialized(pushToken)
{
	var pushNotification = window.plugins.pushNotification;
	pushNotification.getTags(
		function(tags) {
			console.warn('tags for the device: ' + JSON.stringify(tags));
		},
		function(error) {
			console.warn('get tags error: ' + JSON.stringify(error));
		}
	);

	pushNotification.getPushToken(
		function(token)
		{
			console.warn('push token device: ' + token);
		}
	);

	pushNotification.getPushwooshHWID(
		function(token) {
			console.warn('Pushwoosh HWID: ' + token);
		}
	);

}
