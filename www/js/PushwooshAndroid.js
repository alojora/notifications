

function registerPushwooshAndroid() {

 	var pushNotification = window.plugins.pushNotification;

	//set push notifications handler
	document.addEventListener('push-notification',
		function(event)
		{
            var title = event.notification.title;
            var userData = event.notification.userdata;


            if(typeof(userData) != "undefined") {
				console.warn('user data: ' + JSON.stringify(userData));
			}


			alert(title);


		}
	);

	pushNotification.onDeviceReady({ projectid: "370001790729", appid : "4CCD5-11B0C" });

	pushNotification.registerDevice(
		function(token)
		{
			alert(token);
			onPushwooshAndroidInitialized(token);
		},
		function(status)
		{
			alert("failed to register: " +  status);
		    console.warn(JSON.stringify(['failed to register ', status]));
		}
	);
}

function onPushwooshAndroidInitialized(pushToken)
{
	console.warn('push token: ' + pushToken);

	var pushNotification = window.plugins.pushNotification;

	pushNotification.getPushToken(
		function(token)
		{
			console.warn('push token: ' + token);
		}
	);

	pushNotification.getPushwooshHWID(
		function(token) {
			console.warn('Pushwoosh HWID: ' + token);
		}
	);

	pushNotification.getTags(
		function(tags)
		{
			console.warn('tags for the device: ' + JSON.stringify(tags));
		},
		function(error)
		{
			console.warn('get tags error: ' + JSON.stringify(error));
		}
	);




	pushNotification.setLightScreenOnNotification(false);


	pushNotification.setTags({deviceName:"hello", deviceId:10},
		function(status) {
			console.warn('setTags success');
		},
		function(status) {
			console.warn('setTags failed');
		}
	);


}
